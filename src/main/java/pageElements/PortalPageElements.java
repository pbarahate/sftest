package pageElements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PortalPageElements {

WebDriver driver;
	
	@FindBy(xpath = "/html[1]/body[1]/div[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[11]/div[3]/a[1]") public WebElement changeLink;
	@FindBy(xpath="//div[@class='slds-form-element display-element']/descendant::div[@data-name='Controlling__c-input']") public WebElement controlling;
	@FindBy(xpath="//div[@class='slds-form-element display-element']/descendant::div[@data-name='Dependent__c-input']") public WebElement dependent;
	@FindBy(xpath = "//button[contains(@data-name,'saveBtn')]") public WebElement saveButton;
	
	public PortalPageElements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
}
