package pageElements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BackEndUserProPageElements {

	WebDriver driver;

	@FindBy(xpath = "//input[@name='username']")
	public WebElement bckUsername;
	@FindBy(xpath = "//input[@name='pw']")
	public WebElement bckPassword;
	@FindBy(xpath = "//input[@id='Login']")
	public WebElement login;
	@FindBy(xpath = "//a[@data-label='Details']")
	public WebElement contactDetailsPage;
	@FindBy(xpath = "//span[@class='test-id__field-label'][contains(.,'Controlling')]//following::lightning-formatted-text")
	public WebElement bckEndControllingValue;
	@FindBy(xpath = "//span[@class='test-id__field-label'][contains(.,'Dependent')]//following::lightning-formatted-text")
	public WebElement bckEndDependentValue;
	@FindBy(xpath = "//span[@class='label'][contains(.,'Create')]")
	public WebElement bckHomeScreen;

		
	public BackEndUserProPageElements(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
