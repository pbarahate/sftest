package pages;

import static org.testng.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import pageElements.LoginPageElements;
import pageElements.PortalPageElements;

public class PortalPage extends BasePage {

	String controllingValue;

	PortalPageElements portalPage;

	public PortalPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		portalPage = new PortalPageElements(this.driver);

	}

	public void clickChangeLink() {

		wait.until(ExpectedConditions.elementToBeClickable(portalPage.changeLink));
		WebElement changeLink = portalPage.changeLink;
		changeLink.click();
	}

	public void clickControlling(String controllingValue1) {

		wait.until(ExpectedConditions.elementToBeClickable(portalPage.controlling));
		WebElement controlling = portalPage.controlling;
		controlling.click();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//option[@value='" + controllingValue1 + "']")));
		driver.findElement(By.xpath("//option[@value='" + controllingValue1 + "']")).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(portalPage.controlling));
		WebElement controlling1 = portalPage.controlling;
		controlling1.click();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//option[@value='" + controllingValue1 + "']")));
		driver.findElement(By.xpath("//option[@value='" + controllingValue1 + "']")).click();

		controllingValue = controllingValue1;

	}

	public void verifyDependent(String dependentValue) {

		wait.until(ExpectedConditions.elementToBeClickable(portalPage.dependent));
		WebElement dependent = portalPage.dependent;
		dependent.click();
		
		System.out.println("==============controllingValue====="+controllingValue);
		System.out.println("==============dependentValue====="+dependentValue);
		
		//Assert.isTrue(dependentValue.equals(controllingValue), "Preferred name not updated");
		
		if (dependentValue == dependentValue || controllingValue == controllingValue ) {

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//option[@value='" + dependentValue + "']")));
			WebElement depVal = driver.findElement(By.xpath("//option[@value='" + dependentValue + "']"));
			depVal.click();

			portalPage.saveButton.click();
		} 
		
		else {
			Assert.fail("The Dependent value not macted with thw expected value");
		}

	}

}
