package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import pageElements.BackEndUserProPageElements;
import pageElements.PortalPageElements;

public class BackEndUserProPage extends BasePage {

	BackEndUserProPageElements backEndUserProPage;

	public BackEndUserProPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		backEndUserProPage = new BackEndUserProPageElements(this.driver);

	}

	public void bckEndUrl (String url) {
		driver.get(url);
		
	}
	public void bckEndUsername(String username) {
		
		
		backEndUserProPage.bckUsername.sendKeys(username);
	}
	
	public void bckEndPassword(String password) {
		backEndUserProPage.bckPassword.sendKeys(password);
		backEndUserProPage.login.click();
	}
	
	public void contactDetailsScreen(String url) {
		wait.until(ExpectedConditions.elementToBeClickable(backEndUserProPage.bckHomeScreen));
		driver.get(url);
		wait.until(ExpectedConditions.elementToBeClickable(backEndUserProPage.contactDetailsPage));
		backEndUserProPage.contactDetailsPage.click();
		
	}
	
	public void verifyPicklistValues(String dependentVal, String controlingVal) {
		
		
		
		 wait.until(ExpectedConditions.elementToBeClickable(backEndUserProPage.bckEndControllingValue));
		 WebElement contfirlValurBackEnd = backEndUserProPage.bckEndControllingValue;
		 String val1 =contfirlValurBackEnd.getText();
		 
		 wait.until(ExpectedConditions.elementToBeClickable(backEndUserProPage.bckEndDependentValue));
		 WebElement depnfirlValurBackEnd = backEndUserProPage.bckEndDependentValue;
		 String dep1 = depnfirlValurBackEnd.getText();
		 System.out.println("======The Passed con Value====="+controlingVal);
		 System.out.println("======The Passed dep Value====="+dependentVal);
		 System.out.println("====The actual depenedent piclist value in BackEnd is:::::"+dep1);
		 System.out.println("====The actual controlling piclist value in BackEnd is:::::"+val1);
		 
		 Assert.assertEquals(dep1, dependentVal);
			Assert.assertEquals(val1, controlingVal);
		 
		/* if (dependentVal == dep1 & controlingVal == val1) {
			 
			 System.out.println("The picklist's Values are updated properly in the BackEnd");
		 }else {
			 Assert.fail("Alert::::The picklist's Values are not updated properly in the BackEnd");
		 }*/
	}
	
}
