package test;

import org.testng.annotations.Test;

import pages.BackEndUserProPage;
import pages.LoginPage;
import pages.PortalPage;
import reuseable.PropertiesOperation;

public class LoginTest extends	AbstractBaseClass {

		@Test
		public void verifyPicklistValues() throws Exception {

			
			String controllingVal = PropertiesOperation.getPropertiesValues("controllingVal");;
			String dependentVal = PropertiesOperation.getPropertiesValues("dependentVal");
			String portalurl = PropertiesOperation.getPropertiesValues("portalurl");
			String portalusername = PropertiesOperation.getPropertiesValues("portalusername");
			String portalpassword = PropertiesOperation.getPropertiesValues("portalpassword");
			String bckEndContactDetailsSection = PropertiesOperation.getPropertiesValues("bckEndContactDetailsSection");
			String bckEndUsername = PropertiesOperation.getPropertiesValues("bckEndUsername");
			String bckEndPassword = PropertiesOperation.getPropertiesValues("bckEndPassword");
			String bckEndUrl = PropertiesOperation.getPropertiesValues("bckEndUrl");
			
			LoginPage lp = new LoginPage(driver);
			PortalPage pp = new PortalPage(driver);
			BackEndUserProPage bp = new BackEndUserProPage(driver);
			
			driver.get(portalurl);
			lp.enterUserName(portalusername);
			lp.enterPassword(portalpassword);
			lp.clickSubmit();
			pp.clickChangeLink();
			pp.clickControlling(controllingVal);
			pp.verifyDependent(dependentVal);
			bp.bckEndUrl(bckEndUrl);
			bp.bckEndUsername(bckEndUsername);
			bp.bckEndPassword(bckEndPassword);
			bp.contactDetailsScreen(bckEndContactDetailsSection);
			bp.verifyPicklistValues(dependentVal, controllingVal);
			
			
			
		}	

}
