package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import java.net.URL;
import org.testng.annotations.BeforeMethod;

public class AbstractBaseClass {

	public static final String USERNAME = "fswebmaster1";
	public static final String AUTOMATE_KEY = "aWpKeyj2iZtjqu2VUd5M";
	public static String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	
	WebDriver driver;
	String url;
	String browDriverPath = System.getProperty("user.dir")+"/BrowserDrivers/geckodriver.exe";
	 
	@BeforeMethod
	public void setUp() {
		
						
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("browser", "Firefox");
			caps.setCapability("browser_version", "latest");
			caps.setCapability("browserstack.debug", "true");
			caps.setCapability("os", "Windows");
			caps.setCapability("os_version", "10");
			caps.setCapability("resolution", "1024x768");

			driver = new RemoteWebDriver(new URL(URL), caps);
			
			driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);

			} catch (Exception e) {

		}
		 /*System.setProperty("webdriver.gecko.driver", browDriverPath);
		 driver = new FirefoxDriver();
		  driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);*/
		  //driver.manage().window().maximize();
	}

		
	public void tearDown() {
		driver.quit();
	}
	
	
}
